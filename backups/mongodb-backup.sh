#!/usr/bin/env bash

set -e

########################################
###==================================###
### MongoDB Backup                   ###
### Author : Alban ESPIE-GUILLON     ###
### alban.espie@alterway.fr          ###
### AlterWay Hosting                 ###
###==================================###
########################################

## {{{ Global variables
##------------------##
## Global Variables ##
##------------------##
SUBJECT="mongodb-backup"
VERSION="1.2.1 (07/11/2019)"
USAGE="Usage: ${0} -hv -u [username] -p [password] -o [out] -d [database] -c [collections] \n
-h : display help. \n
-v : display version. \n
-u : username, use root by default.\n
-p : password, use the password in /root/mongorc.js by default. \n
-o : out, specify the directory used to store backups. \n
-d : database, select one database to backup. Backup everything by default \n
-c : collections (or tables) to backup, need -d parameter to specify from which database. You can use -c option multiple times to backup several collections from a database."
HELP="This script is used to backup mongodb. It was originally written in order to be used for automatic backups systems."
LOG="/var/log/${SUBJECT}.log"
DATE=$(date --iso-8601=seconds)
## Global variables }}}

##--------------------------------------------------------##
## Script Variables - THE ONLY THING YOU MAY NEED TO EDIT ##
##--------------------------------------------------------##
BKPDIR='/var/backups/mongodb'
MONGOUSERNAME='root'
MONGO_PWD_FILE='/root/.mongorc.js'
BKP_OPTIONS=''

##--------------------------------------------------------------------------------------------------------------------------------##
## Don't touch this part ---------------------------------------------------------------------------------------------------------##
##--------------------------------------------------------------------------------------------------------------------------------##
## {{{ Script miscellaneous
## Root Check ##
if [ ${UID} -ne "0" ]
then
   echo "[${DATE}] You need to be root to execute this script." >/dev/stderr
   exit 1
fi

# SECONDS returns a count of the number of (whole) seconds the shell has been running.
startTime=${SECONDS}
## Script miscellaneous }}}

## {{{ Option processing
##-------------------##
## Option processing ##
##-------------------##
while getopts ":vhu:p:o:d:c:" optname
  do
    case ${optname} in
      "v")
        echo "Version ${VERSION}"
        exit 0;
        ;;
      "h")
        echo -e ${HELP}
        echo -e ${USAGE}
        exit 0;
        ;;
      "d") # Database
        d=${OPTARG}
        ;;
      "c") # Collections (Tables)
        c+=("${OPTARG}")
        ;;
      "u") # Username, if parameter does not exist use default
        MONGOUSERNAME=${OPTARG}
        ;;
      "p") # Password
        p=${OPTARG}
        ;;
      "o") # Out : Backup directory, if parameter does not exist use default
        BKPDIR=${OPTARG}
        ;;
      "?")
        echo "Unknown option ${OPTARG}"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))
## Option processing }}}

# -----------------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
# -----------------------------------------------------------------

## Lockfile
(flock -x 200 || exit 1

## If $c parameter (collections / tables) is specified but without $d, print an error and exit
if [[ -n ${c} ]] && [[ -z ${d} ]] ; then
  echo "Error : Cannot dump a collection without a specified database." >/dev/stderr
  exit 1
fi

# Check mongodump version to see if '--gzip' option is available
if [[ $(echo "$(mongodump --version | head -n 1 | awk '{print $NF}' | sed 's/^[a-Z]//' | cut -c 1-3) >= 3.2" | bc -l) ]]; then
  BKP_OPTIONS='--gzip'
fi

## Use password parameter if exist, if not, try to get the password from ${MONGO_PWD_FILE}
if [[ -n ${p} ]] ; then
  MONGOPASSWD=${p}
elif [[ -f ${MONGO_PWD_FILE} ]] && [[ -s ${MONGO_PWD_FILE} ]] ; then
  MONGOPASSWD=$( grep db.getSiblingDB ${MONGO_PWD_FILE} | egrep -o '\"[[:alnum:]]+\"\)\;' | egrep -o '[[:alnum:]]+' )
else
  echo "File ${MONGO_PWD_FILE} does not exist or is empty." >/dev/stderr
fi

## Mkdir backup directory
mkdir -p ${BKPDIR}

## if database is specified
## 2>&1 | grep -Ev "done dumping|writing" : redirect everything to stdout and do not display successful logs (we wants only errors)
if [[ -n ${d} ]] ; then
  ## If database and collections (tables) are specified then backup
  if [[ -n ${d} ]] && [[ -n ${c} ]] ; then
    for collection in ${c[@]}; do
      mkdir -p ${BKPDIR}/${d}/${collection}
      mongodump ${BKP_OPTIONS} -u ${MONGOUSERNAME} -p ${MONGOPASSWD} -o ${BKPDIR}/${d}/${collection} -d ${d} -c ${collection}
    done
  else
    ## Else, only backup the database
    mongodump ${BKP_OPTIONS} -u ${MONGOUSERNAME} -p ${MONGOPASSWD} -o ${BKPDIR} -d ${d}
  fi
## If getting password through parameter or through ${MONGO_PWD_FILE} failed, try to backup without authentification
elif [[ -z ${MONGOPASSWD+x} ]]; then
  mongodump ${BKP_OPTIONS} -o ${BKPDIR}
else
  ## Else, backup everything
  mongodump ${BKP_OPTIONS} -u ${MONGOUSERNAME} -p ${MONGOPASSWD} -o ${BKPDIR}
fi

## Display backup time
elapsedTime=$((${SECONDS} - ${startTime}))
echo "Backup duration : $((${elapsedTime}/60)) min $((${elapsedTime}%60)) sec"

)200>/var/lock/${SUBJECT}.lock

exit 0
