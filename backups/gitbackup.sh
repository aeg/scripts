#!/usr/bin/env bash
set -e

PATH=/bin:/sbin:/usr/bin:/usr/sbin

###==================================###
### Git Backup Script                ###
### Author : Alban ESPIE-GUILLON     ###
###==================================###

## {{{ Global variables
##------------------##
## Global Variables ##
##------------------##
SUBJECT="gitbackup"
VERSION="1.0.0 (03/09/2019)"
USAGE="Usage: ${0} -hv -p [path] \n
-h : display help. \n
-v : display version. \n
-p : path to the project directory (use absolute path). \n
-b : repository branch (optional, default to master). \n
-e : email to use to send warning if script failed (optional, default to root)."
HELP="This script is used to backup stuff through git. The repository must already be initialized."
LOG="/var/log/${SUBJECT}.log"
DATE=$(date '+%F-%Hh')
## Global variables }}}

## {{{ Option processing
##-------------------##
## Option processing ##
##-------------------##
while getopts ":vhp:b:e:" optname
  do
    case ${optname} in
      "v")
        echo "Version ${VERSION}"
        exit 0;
        ;;
      "h")
        echo ${HELP}
        echo -e ${USAGE}
        exit 0;
        ;;
      "p") # Path
        p=${OPTARG}
        ;;
      "b") # Branch
        b=${OPTARG}
        ;;
      "e") # Admin email
        e=${OPTARG}
        ;;
      "?")
        echo "Unknown option ${OPTARG}"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))
## Option processing }}}

# -----------------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
# -----------------------------------------------------------------

## Use path parameter if exist, if not exit
if [[ -n ${p} ]] ; then
  REPOSITORY=${p}
else
  echo "No directory variable available. Exiting."
  exit 1
fi

## Use branch parameter if exist, if not use default
if [[ -n ${b} ]] ; then
  BRANCH=${b}
else
  BRANCH="master"
fi

## Use admin_email parameter if exist, if not use default
if [[ -n ${e} ]] ; then
  ADMIN_EMAIL=${e}
else
  ADMIN_EMAIL="root"
fi

## Lockfile
(flock -x 200 || exit 1

## Clean old log, and log every output ##
echo > ${LOG}
exec &>> ${LOG}

git -C ${REPOSITORY} add .
git -C ${REPOSITORY} commit -m "Auto Git backup by ${USER} on ${HOSTNAME} - ${DATE}"
git -C ${REPOSITORY} push origin ${BRANCH}
result=${?}

# Send mail if failed
if [[ ${result} != 0 ]]; then
  cat ${LOG} | mail -s "[Auto Git Backup - $(hostname)] May have failed" ${ADMIN_EMAIL}
fi

)200>/var/lock/${SUBJECT}.lock

exit 0
