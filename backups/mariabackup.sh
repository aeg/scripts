#!/bin/sh
# shellcheck disable=SC2086
set -e #stops execution if a variable is not set
set -u #stop execution if something goes wrong

# This script is compatible until MariaDB 10.3 because of the --compress option.

#######################################
## FUNCTIONS
#

die () {
    echo "$1" >&2
    exit 1
}

usage() {
    echo "usage: $(basename "${0}") [-h|--help] [--cron] [--full|--incremental|--prepare]"
    echo "   --help: show this help."
    echo "   --cron: redirect all outputs to logfile (in ${BACKUP_BASE_DIR}/X/logs) - not usable with --prepare."
    echo "   --full: do a full backup, approx time 5min."
    echo "   --incremental: do an incremental backup."
    echo "   --prepare: prepare backup. Requires a non-empty argument containing the path of the backup to prepare."
    echo "              Example: $(basename "${0}") --prepare 2019-11-19/inc2"
    echo "                     : $(basename "${0}") --prepare=2019-11-19/full"
}

full_backup() {
    # Create $BACKUP_DIR and logdir
    mkdir -p "${BACKUP_DIR}"/logs

    if [ -d "${BACKUP_DIR}"/full ]
    then
        die "ERROR: ${BACKUP_DIR}/full already exists, not overwriting."
    fi

    # Redirect to logfile if --cron
    if [ "$cron" -eq 1 ]
    then
        exec 1>"${BACKUP_DIR}"/logs/full.log
        exec 2>&1
    fi

    # Classic full backup, nothing fancy
    echo "INFO: [$(date)] Starting Full backup"
    $MARIABACKUP_BIN $MARIABACKUP_BACKUP_ARGS --target-dir="${BACKUP_DIR}"/full
    echo "INFO: [$(date)] Ending Full backup"
}

incremental_backup() {
    # We are using a spool file to get the last usable incremental (the last finished one)
    if [ ! -f "${BACKUP_DIR}"/mariabackup_auto-last_incremental ]; then
        NUMBER=1
    else
        NUMBER=$(($(cat "${BACKUP_DIR}"/mariabackup_auto-last_incremental) + 1))
    fi

    # Redirect to logfile if --cron
    if [ "$cron" -eq 1 ]
    then
        exec 1>>"${BACKUP_DIR}"/logs/inc${NUMBER}.log
        exec 2>&1
    fi

    # We need a full for the incremental base
    if [ ! -d "${BACKUP_DIR}"/full ]
    then
        die "ERROR: no full backup has been done today. Aborting."
    fi

    # Remove any failed incremental dir (backup dir created but spool file not edited)
    if [ -d "${BACKUP_DIR}"/inc${NUMBER} ]
    then
        echo "WARN: Removing existing directory ${BACKUP_DIR}/inc${NUMBER} (backup may be corrupted as the spool file has not been edited)" >&2
        rm -rf "${BACKUP_DIR}"/inc${NUMBER}
    fi

    # Incremental backup (based on full if it's the first one, else based on the last incremental)
    echo "INFO: [$(date)] Starting Incremental backup no. $NUMBER"
    if [ $NUMBER -eq 1 ]
    then
        $MARIABACKUP_BIN $MARIABACKUP_BACKUP_ARGS --target-dir="${BACKUP_DIR}"/inc${NUMBER} --incremental-basedir="${BACKUP_DIR}"/full
    else
        $MARIABACKUP_BIN $MARIABACKUP_BACKUP_ARGS --target-dir="${BACKUP_DIR}"/inc${NUMBER} --incremental-basedir="${BACKUP_DIR}"/inc$(( NUMBER - 1 ))
    fi
    echo "INFO: [$(date)] Ending Incremental backup no. $NUMBER"

    # Edit the spool file
    echo $NUMBER > "${BACKUP_DIR}"/mariabackup_auto-last_incremental
}

prepare() {
    TARGET_BACKUP_DIR=${BACKUP_BASE_DIR}/$(echo "$1" | cut -d'/' -f1)
    RESTORE_DIR=${TARGET_BACKUP_DIR}_restore
    BACKUP_TO_RESTORE=$(echo "$1" | cut -d'/' -f2)

    # Check if a restore has already been tried for this day
    if [ -d "${RESTORE_DIR}" ]
    then
        die "ERROR: ${RESTORE_DIR} already exists, not overwriting."
    fi

    # Check if full & $BACKUP_TO_RESTORE exist
    if [ ! -d "${TARGET_BACKUP_DIR}"/full ]
    then
        die "ERROR: no full backup is available in ${TARGET_BACKUP_DIR}/full. Aborting."
    fi

    if [ ! -d "${TARGET_BACKUP_DIR}"/"${BACKUP_TO_RESTORE}" ]
    then
        die "ERROR: '$BACKUP_TO_RESTORE' No such backup in ${TARGET_BACKUP_DIR}. Aborting."
    fi

    echo "INFO: Preparing $1 for restore."
    # Copy the midnight full dir to restore_base to keep an intact full, just in case
    cp -r "${TARGET_BACKUP_DIR}" "${RESTORE_DIR}"
    echo "      You can now keep track of what's happening in another shell with :"
    echo "         tail -f ${RESTORE_DIR}/logs/prepare.log"

    # Decompress + prepare the midnight full
    $MARIABACKUP_BIN $MARIABACKUP_DECOMPRESS_ARGS --target-dir="${RESTORE_DIR}"/full >> "${RESTORE_DIR}"/logs/prepare.log 2>&1
    $MARIABACKUP_BIN $MARIABACKUP_PREPARE_ARGS --target-dir="${RESTORE_DIR}"/full >> "${RESTORE_DIR}"/logs/prepare.log 2>&1

    # Decompress + prepare incrementals (if we want to restore an incremental)
    case $BACKUP_TO_RESTORE in
        inc[1-9]|inc1[0-9]|inc2[0-3])
            i=1
            while [ $i -le "$(echo "$BACKUP_TO_RESTORE" | sed s/inc//)" ]
            do
                $MARIABACKUP_BIN $MARIABACKUP_DECOMPRESS_ARGS --target-dir="${RESTORE_DIR}"/inc"$i" >> "${RESTORE_DIR}"/logs/prepare.log 2>&1
                $MARIABACKUP_BIN $MARIABACKUP_PREPARE_ARGS --target-dir="${RESTORE_DIR}"/full \
                    --incremental-dir="${RESTORE_DIR}"/inc$i >> "${RESTORE_DIR}"/logs/prepare.log 2>&1
                i=$(( i+1 ))
            done
            ;;
    esac

    echo "INFO: Backup $1 has been prepared in ${RESTORE_DIR}/full, you can now start the actual restore."
    echo "INFO: Restore can be done by stopping the 2 other nodes and then :"
    echo "      mariabackup --move-back --target-dir=${RESTORE_DIR}/full"
    echo "      chown -R mysql: /var/lib/mysql"
    echo "      galera_new_cluster"
}

#######################################
## VARIABLES
#

DATE=$(date +%Y-%m-%d)
BACKUP_BASE_DIR=/var/backups/databases/mariadb
BACKUP_DIR=${BACKUP_BASE_DIR}/${DATE}
MARIABACKUP_BIN=/usr/bin/mariabackup
MARIABACKUP_BACKUP_ARGS='--backup --rsync --parallel=4 --galera-info --compress --compress-threads=4'
MARIABACKUP_DECOMPRESS_ARGS='--decompress --remove-original --parallel=4'
MARIABACKUP_PREPARE_ARGS='--prepare --parallel=4'
cron=0 # cf --cron

#######################################
## MAIN
#

if [ $# -eq 0 ]
then
    usage
    exit 1
fi
while :; do
    case $1 in
        -h|--help)
            usage
            exit
            ;;
        --cron) # triggue le exec &>LOGFILE
            cron=1
            ;;
        --full)
            full_backup
            exit
            ;;
        --incremental)
            incremental_backup
            exit
            ;;
        --prepare) # --prepare $path
            if [ "$2" ]
            then
                prepare "$2"
            else
                die 'ERROR: "--prepare" requires a non-empty option argument'
            fi
            exit
            ;;
        --prepare=?*) # --prepare=$path
            prepare "${1#*=}"
            exit
            ;;
        --prepare=) # cas du --prepare= sans argument
            die 'ERROR: "--prepare" requires a non-empty option argument'
            ;;
        --)
            shift
            break
            ;;
        -?*)
            echo "WARN: Unknown option (ignored): $1" >&2
            ;;
        *)
            break
    esac
    shift
done
