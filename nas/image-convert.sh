#!/usr/bin/env bash
set -euo pipefail

#
# Alban E.G.
# 2021.07.18
# This script is used to convert image files to jpg (default).
# Require at least imagemagick. Other known dependencies : ufraw for RW2 files.
#

SUBJECT=$(basename ${0})
USAGE="Usage: ${SUBJECT} -h [source extension] [target extension]"
HELP="This script is used to convert image files (source) to another image type (target) with imagemagick, using jpg as default."

## {{{ Option processing
if [ $# == 0 ] ; then
		echo ${HELP}
		echo ${USAGE}
    exit 1;
fi

while getopts ":h" optname
  do
    case "${optname}" in
      "h")
        echo ${HELP}
        echo ${USAGE}
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))
## Option processing }}}

## Lockfile
(flock -x 200 || exit 1

if [[ -n ${1} ]] ; then
  SOURCE=${1}
else
  echo "This script needs an extension target as argument, exiting."
  exit 1
fi

TARGET=${2:-'jpg'}

echo "Using ${SOURCE} as source extension and ${TARGET} as target extension."

for item in $(pwd)/*.${SOURCE}; do
	BASENAME=$(basename ${item})
	convert "${item}" ${BASENAME}.${TARGET}
done

)200>/var/lock/${SUBJECT}.lock

exit 0
