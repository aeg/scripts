#!/usr/bin/env bash
set -euo pipefail

#
# Alban E.G.
# 2021.07.18
# This script is used to compress videos files with ffmpeg.
# Require ffmpeg binary.
#

SUBJECT=$(basename ${0})
USAGE="Usage: ${SUBJECT} -h -e [target extension] -f [target file]"
HELP="This script is used to compress videos files with ffmpeg."

compress_with_extension() {
	if [[ -n ${1} ]] ; then
		EXTENSION=${1}
	else
		echo "This script needs an extension target as argument, exiting."
		exit 1
	fi

	(flock -x 200 || exit 1
	for item in $(pwd)/*.${EXTENSION}; do
		time ffmpeg -i "$(basename ${item})" -vcodec libx265 -crf 28 OPTIMIZED."$(basename ${item})"
	done
	)200>/var/lock/${SUBJECT}-${EXTENSION}.lock
}

compress_with_file() {
	if [[ -n ${1} ]] ; then
		FILE=${1}
	else
		echo "This script needs a file target as argument, exiting."
		exit 1
	fi

	(flock -x 200 || exit 1
	time ffmpeg -i ${FILE} -vcodec libx265 -crf 28 OPTIMIZED.${FILE}
	)200>/var/lock/${SUBJECT}-${FILE}.lock
}

## {{{ Option processing
if [ $# == 0 ] ; then
		echo ${HELP}
		echo ${USAGE}
    exit 1;
fi

while getopts ":he:f:" optname
  do
    case "${optname}" in
      "h")
        echo ${HELP}
        echo ${USAGE}
        exit 0;
        ;;
			"e")
				compress_with_extension ${OPTARG}
				exit 0;
				;;
			"f")
				compress_with_file ${OPTARG}
				exit 0;
				;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))
## Option processing }}}
