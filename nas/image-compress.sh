#!/usr/bin/env bash
set -euo pipefail
#
# Alban E.G.
# 2021.07.18
# This script is used to compress jpg and png image files, using jpegoptim and optipng.
#

SUBJECT=$(basename ${0})
USAGE="Usage: ${SUBJECT} -h -j -p"
HELP="This script is used to compress jpg and png image files with jpegoptim and optipng. Compress both by default. \n
-h : Help \n
-j : Compress jpg only \n
-p : Compress png only"

compress_jpg() {
	for extension in jpg JPG jpeg JPEG; do
		find "$(pwd)" -name "*.${extension}" -exec jpegoptim -t -v -s -m 80 '{}' \;
	done
}

compress_png() {
	for extension in png PNG; do
		find "$(pwd)" -name "*.${extension}" -exec optipng -v -o5 -fix -strip all '{}' \;
	done
}

while getopts ":hjp" optname
  do
    case "${optname}" in
      "h")
        echo -e ${HELP}
        echo ${USAGE}
        exit 0;
        ;;
			"j")
        compress_jpg
        exit 0;
        ;;
			"p")
        compress_png
        exit 0;
        ;;
      *)
        compress_jpg
				compress_png
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))
