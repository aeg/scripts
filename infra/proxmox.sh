#!/bin/bash

# Script dont l'objectif est d'obtenir la charge déjà allouée sur l'hyperviseur local

output_cache=/tmp/proxmox.sh_status_cache

# We check if cache file exists and is newer than 1 minute
if [ -f $output_cache ]; then
    if [ $(( $(date +%s) - $(stat -L --format %Y $output_cache) )) -lt 60 ]; then
        cat $output_cache
        exit 0
    fi
fi

SRC=/etc/pve/qemu-server
tot_vcpu=0
tot_mem=0
vms=""
qm_list="/usr/sbin/qm list"

# On parcours l'ensemble des fichiers
while read l; do
    # On récupère les variables grepées et on les stocke
    eval $(grep -E "memory|cores|socket|name|net0" $l | sed "s/: /=/")
    vcpu=$(( $cores * $sockets))
    tot_vcpu=$(( $tot_vcpu + $vcpu ))
    tot_mem=$(( $tot_mem + $memory ))
    mac=$(echo $net0 | cut -f 2 -d '=' | cut -f 1 -d ',')
    status=$(echo $qm_list| awk "/$name/ { print \$3 }")
    vm="{\"name\":\"$name\", \"vcpu\":$vcpu, \"mem\":$memory, \"mac\":\"$mac\", \"status\":\"$status\"}"
    if [ "$vms" == "" ]; then
        vms=$vm
    else
        vms="$vms,$vm"
    fi
done <<< "$(ls $SRC/*.conf)"
echo "proxmox_vms=[$vms]" > $output_cache
echo "proxmox_vcpu_total=$tot_vcpu/$(awk '/processor/ {processor=$3} END {print processor + 1}' /proc/cpuinfo)" >> $output_cache
echo "proxmox_mem_total=$tot_mem/$(free -m  | grep Mem | awk '{print $2}')" >> $output_cache

cat $output_cache
