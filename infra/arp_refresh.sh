#!/bin/bash

for DEV in `/sbin/ip address list | /bin/grep mtu | /usr/bin/cut -d':' -f2 | /usr/bin/cut -d'@' -f1` ; do
  for ip in `/sbin/ip address list ${DEV} | /bin/grep "inet " | /usr/bin/awk '{print $2}' | /usr/bin/cut -d'/' -f1 | /bin/grep "\(10.0\|172.16\|192.168\)" ` ; do
    echo "ARPING broadcast for ${ip} to ${DEV}"
    /usr/sbin/arping -S ${ip} -c 1 -w 2 -i ${DEV} -B &> /dev/null
  done
done
